const inventory = require('./info.cjs');

function problem4(inventory){
    if(arguments.length < 1  || inventory.length === 0 || !Array.isArray(inventory)){
        return [];
    }
    const lisyears = [];
    for(let index = 0;index < inventory.length; index++){
        lisyears.push(inventory[index]['car_year']);
    }
    return lisyears;
}

module.exports =  problem4 

