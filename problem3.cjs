const  inventory = require('./info.cjs');



function problem3(inventory){
    if(arguments.length < 1 || inventory.length === 0 || !Array.isArray(inventory)){
        return [];
    }
    const lis = [];
    for(let index = 0; index < inventory.length; index++){
        lis.push(inventory[index]['car_model']);
    }
    lis.sort();
    return lis;

}
module.exports = problem3

 