const problem1 =  require('../problem1.cjs');
const  inventory = require('../info.cjs');

test('provide the index of id',() => {
  expect(problem1(inventory,33)).toStrictEqual({"id":33,"car_make":"Jeep","car_model":"Wrangler","car_year":2011})
})
