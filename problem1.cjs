const inventory = require('./info.cjs');


// console.log(`Car 33 is a ${inventory[result]['car_year']}  , ${inventory[result]['car_make']} , ${inventory[result]['car_model']}`);
function problem1(inventory,cid){
    if(arguments.length !== 2 || inventory.length===0 || !Array.isArray(inventory) || typeof cid !== 'number'){
        return [];
    }
    for(let index = 0; index < inventory.length; index++){

        if( inventory[index]['id'] === cid){
            return inventory[index];
        }
    }

    return [];
    
}
module.exports = problem1 

