
function problem5(result){
    if (arguments.length < 1 || result.length === 0 || !Array.isArray(result)){
        return 0;
    }
    let cars2k = [];
    for(let index = 0;index < result.length; index++){
        if(result[index] < 2000){
            cars2k.push(result[index]);
        }
    }
    return cars2k.length;
}
module.exports = problem5

